#include "FresnoBaseProjectApp.h"

FresnoBaseProjectApp::FresnoBaseProjectApp() {
	effectID = 52;
}

void FresnoBaseProjectApp::InitDevMode() {
	myTexture = loadImage(loadAsset("dot.png"));
	setCanvasSizeVars(getWindowSize().x, getWindowSize().y);

	umbrellaPositions.push_back(Point(0, 0));
	umbrellaPositions.push_back(Point(0.25f, 0.25f));
	umbrellaPositions.push_back(Point(-0.25f, 0.25f));
	umbrellaPositions.push_back(Point(0.25f, -0.25f));
	umbrellaPositions.push_back(Point(-0.25f, -0.25f));
}

void FresnoBaseProjectApp::setCanvasSizeVars(float w, float h) {
	canvasWidth = w;
	canvasHeight = h;
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;
	// dump comment
}

//should only occur in dev (standalone) mode
void FresnoBaseProjectApp::setup()
{
#ifdef DEV_MODE
	InitDevMode();
	globalSetup();
#endif

}

void FresnoBaseProjectApp::mouseDown( MouseEvent event )
{
}

void FresnoBaseProjectApp::update()
{
	Update();
}

void FresnoBaseProjectApp::draw()
{
	Draw();
}

void FresnoBaseProjectApp::Update() {
	setDeltaTime();

	counter++;
	redVal = sin(counter * 0.01f);
	redVal = lmap(redVal, -1.0f, 1.0f, 0.0f, 1.0f);
	blueVal = sin(counter * 0.013f);
	redVal = lmap(blueVal, -1.0f, 1.0f, 0.0f, 1.0f);
	greenVal = sin(counter * 0.017f);
	redVal = lmap(greenVal, -1.0f, 1.0f, 0.0f, 1.0f);

	pe.update(deltaTime);
}

void FresnoBaseProjectApp::setDeltaTime() {
#ifdef DEV_MODE
	float curRecordedTime = cinder::app::getElapsedSeconds();
	deltaTime =  curRecordedTime - lastRecordedTime;
	lastRecordedTime = curRecordedTime;
#else
	float curRecordedTime = privateTimeline->getCurrentTime();
	deltaTime = curRecordedTime - lastRecordedTime;
	lastRecordedTime = curRecordedTime;
#endif
}

void FresnoBaseProjectApp::Draw() {
	isLocked = true;

#ifdef DEV_MODE
	gl::pushMatrices();
	gl::translate(canvasHalfWidth, canvasHalfHeight); 
#endif

	gl::clear(Color(0, 0, 0));

	gl::disable(GL_TEXTURE_2D);
	drawUmbrellaPositions();

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	gl::enableAlphaBlending();

	int mindex = 0;
	std::vector<Point>::iterator PointIterator = mLocations.begin();
	while(PointIterator != mLocations.end()) {
		
		float drawOffsetX = ((PointIterator->GetX() - 0.5) * canvasWidth - float(mSizes[mindex].GetX() * canvasWidth) / 2.0f);
		float drawOffsetY = ((PointIterator->GetY() - 0.5) * canvasHeight - float(mSizes[mindex].GetY() * canvasHeight) / 2.0f);	

		gl::drawSolidRect(Rectf(drawOffsetX, drawOffsetY, drawOffsetX + float(mSizes[mindex].GetX() * canvasWidth), 
			drawOffsetY + float(mSizes[mindex].GetY() * canvasHeight)));

		mindex++;
		std::advance(PointIterator, 1);
	}

	

	gl::disableAlphaBlending();
	gl::enable(GL_TEXTURE_2D);

#ifdef DEV_MODE
	gl::popMatrices();
#endif

	blobCountHack = mindex;
	//drawDebugInfo();
	isLocked = false;
}

void FresnoBaseProjectApp::drawUmbrellaPositions() {
	if (umbrellaPositions.size() == 0) return;

	//glColor4f(1.0f, 0, 0, 1.0f);
	//gl::drawSolidRect(Rectf(0, 0, 150, 150));

	//int mindex = 0;
	//std::vector<Point>::iterator PointIterator = umbrellaPositions.begin();
	//while(PointIterator != umbrellaPositions.end()) {
	//	
	//	float drawOffsetX = ((PointIterator->GetX() - 0.5) * canvasWidth - radius * 0.5f);
	//	float drawOffsetY = ((PointIterator->GetY() - 0.5) * canvasHeight - radius * 0.5f);	

	//	gl::drawSolidRect(Rectf(drawOffsetX, drawOffsetY, drawOffsetX + radius * canvasWidth, drawOffsetY + radius * canvasHeight));

	//	mindex++;
	//	std::advance(PointIterator, 1);
	//}

	float radius = 0.05f * canvasWidth;
	for (int i = 0; i < umbrellaPositions.size(); i++) {
		gl::drawSolidCircle(Vec2f(umbrellaPositions[i].GetX() * canvasWidth, umbrellaPositions[i].GetY() * canvasHeight), radius);
	}
}

void FresnoBaseProjectApp::globalSetup() {

	if(myTexture) {
		pe.setup(myTexture);
		counter = 0;

		int numParticles = 20;
		for (int i = 0; i < numParticles; i++) {
			Particle p(canvasWidth, canvasHeight);
			pe.add(p);
		}
	}

	mFont = Font( "Times New Roman", 96 );
	mTextureFont = gl::TextureFont::create( mFont );
	lastRecordedTime = 0;
	deltaTime = 0;
}

void FresnoBaseProjectApp::splitPairData(const std::string &s, char delimiter) {
	
	stringstream ss;
	ss.str(s);
	string mItem;

	int i=0;
	Point mPoint;

	while(std::getline(ss, mItem, delimiter)) {
		if((i++)==0) {
			mPoint.SetX(atof(mItem.c_str()));
		} else {
			mPoint.SetY(atof(mItem.c_str()));
		}		
	}

	// fill
	pixelCoordinates.push_back(mPoint);

}

int FresnoBaseProjectApp::splitPairs(const std::string &s, char delimiter) {

	vector<string> myPairs;
	stringstream ss;
	ss.str(s);

	string mItem;

	while(std::getline(ss, mItem, delimiter)) {
		splitPairData(mItem, ',');
	}

	return pixelCoordinates.size();

} 

void FresnoBaseProjectApp::Initialize(char* InitData, Timeline* CinderTimeline) {

	privateTimeline = CinderTimeline;

	int length=0;
	while (InitData[length++] != '}'){}

	char* JSON = new char[length];
	for (int i = 0; i < length; i++){
		JSON[i] = InitData[i];
	}
	JSON[length] = '\0';

	rapidjson::Document data;
	data.Parse<0>(JSON);

	canvasWidth = (float)data["canvasWidth"].GetDouble();
	canvasHeight = (float)data["canvasHeight"].GetDouble();
	string myTexturePath = (string)data["texturePath"].GetString();
	string stringCoordinates = (string)data["pixelCoordinates"].GetString();

	if(!stringCoordinates.empty())
		splitPairs(stringCoordinates,'!');

	const char *cstr = myTexturePath.c_str();
	const char *cstr2 = stringCoordinates.c_str();

	std::stringstream ss;
	std::string s;
	ss << std::noskipws << myTexturePath;
	s = ss.str();

	std::ifstream infile(cstr);

	if(infile.good()){
		myTexture = gl::Texture(loadImage(s.c_str()));					
	} else {
		myTexture = gl::Texture(loadImage("DefaultAssets/images/dot.png"));
	}

	umbrellaPositions.push_back(Point(0, 0));
	umbrellaPositions.push_back(Point(0.25f, 0.25f));
	umbrellaPositions.push_back(Point(-0.25f, 0.25f));
	umbrellaPositions.push_back(Point(0.25f, -0.25f));
	umbrellaPositions.push_back(Point(-0.25f, -0.25f));

	setCanvasSizeVars(canvasWidth, canvasHeight);	

	globalSetup();

	ofstream myfile;
	myfile.open ("debug_message.txt");
	myfile << CurrentDateTime();
	myfile.close();

}

void FresnoBaseProjectApp::SetEffectData(char* Data) {

	char delimiter = ';';
	char divider = ',';
	int i = 0;
	int j = 0;

	char r[32]; // float 1 ...
	i = j;
	while (Data[i] != divider){
		r[i - j] = Data[i];
		i++;
		}
	r[i - j] = '\0';
	i++;
	j = i;

	char g[32]; // float 2 ...
	i = j;
	while (Data[i] != divider){
		g[i - j] = Data[i];
		i++;
		}
	g[i - j] = '\0';
	i++;
	j = i;

	char b[32]; // float 3 ...
	i = j;
	while (Data[i] != divider){
		b[i - j] = Data[i];
		i++;
		}
	b[i - j] = '\0';
	i++;
	j = i;

	char opacity[32]; // float 4 ...
	i = j;
	while (Data[i] != divider){
		opacity[i - j] = Data[i];
		i++;
		}
	opacity[i - j] = '\0';
	i++;
	j = i;

	char radius[32]; // float 5 ...
	i = j;
	while (Data[i] != divider){
		radius[i - j] = Data[i];
		i++;
		}
	radius[i - j] = '\0';
	i++;
	j = i;

	char elements[32];
	i = j;
	while (Data[i] != divider){
		elements[i - j] = Data[i];
		i++;
		}
	elements[i - j] = '\0';
	i++;
	j = i;

	int num = (int)atof(elements);

	param1 = atof(r);
	param2 = atof(g);
	param3 = atof(b);
	param4 = atof(opacity);
	param5 = atof(radius);

	mSizes.clear();
	mLocations.clear();

	if(num == 0) return;

	for(int element = 0; element < num; element++) {
		
		char coordinatex[32]; // X
		i = j;
		while (Data[i] != divider){
			coordinatex[i - j] = Data[i];
			i++;
			}
		coordinatex[i - j] = '\0';
		i++;
		j = i;

		char coordinatey[32]; // Y
		i = j;
		while (Data[i] != divider){
			coordinatey[i - j] = Data[i];
			i++;
			}
		coordinatey[i - j] = '\0';
		i++;
		j = i;

		char sizex[32]; // Width (percent)
		i = j;
		while (Data[i] != divider){
			sizex[i - j] = Data[i];
			i++;
			}
		sizex[i - j] = '\0';
		i++;
		j = i;

		char sizey[32]; // Height (percent)
		i = j;
		while (Data[i] != divider){
			sizey[i - j] = Data[i];
			i++;
			}
		sizey[i - j] = '\0';
		i++;
		j = i;

		// center locations
		mLocations.push_back(
			Point(
			(double)atof(coordinatex), 
			(double)atof(coordinatey)));

		// size pairs
		mSizes.push_back(
			Point(
			(double)atof(sizex), 
			(double)atof(sizey)));		
	}
}

Vec2f getWorldCoords(Vec2f normalizedCoords) {
	Vec2f worldCoords;
#ifdef DEV_MODE

#else
#endif

	return worldCoords;
}

void FresnoBaseProjectApp::drawDebugInfo() {
	gl::enableAlphaBlending();
	gl::color( ColorA( 1, 0.0f, 0.0f, 1.0f ) );
	Rectf boundsRect(0, 
			0, 
			500, 
			200 );
	std::string str;
	str = "num umbrellas: ";
	str += ci::toString(umbrellaPositions.size());
	mTextureFont->drawString( str, boundsRect );
}

const std::string FresnoBaseProjectApp::CurrentDateTime() {

    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[12];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);
    return buf;

}

#ifdef DEV_MODE
CINDER_APP_NATIVE( FresnoBaseProjectApp, RendererGl )
#else
extern "C"{
	__declspec(dllexport) IEffectInterface* GetInterface(){
		IEffectInterface * effect = new FresnoBaseProjectApp();
		return effect;
		}}
#endif