#include "ParticleEmitter.h"

void ParticleEmitter::setup(cinder::gl::Texture tex) {
	particleTexture = tex;
}

void ParticleEmitter::add(Particle p) {
	particles.push_back(p);
}

void ParticleEmitter::render() {

	gl::enableAlphaBlending();
	particleTexture.enableAndBind();
	int size = particles.size();

	for (int i = 0; i < size; i++) {
		gl::color( particles[i].getColor() );
		float partSize = particles[i].getSize();
		Vec2f partPos = particles[i].getPosition();
		ci::gl::drawSolidRect(ci::Rectf(
			partPos.x - partSize,
			partPos.y - partSize,
			partPos.x + partSize,
			partPos.y + partSize));
	}

	gl::disableAlphaBlending();

}

void ParticleEmitter::update(float deltaTime) {
	int size = particles.size();
	for (int i = 0; i < size; i++) {
		particles[i].update(deltaTime);
	}
}