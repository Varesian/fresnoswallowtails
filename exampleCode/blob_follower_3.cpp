void BlobFollower::Initialize(char* InitData, Timeline* CinderTimeline) {

	privateTimeline = CinderTimeline;

	int i = 0, j = 0;
	while(InitData[i++] != ',') {}
	j = i;
	
	char ParentWidth[8], ParentHeight[8];	

	for(i; i < (j + 6); i++) {
		ParentWidth[i - j] = InitData[i];
	}
	ParentWidth[6] = '\0';
	j = i;

	for(i; i < (j + 6); i++) {
		ParentHeight[i - j] = InitData[i];
	}
	ParentHeight[6] = '\0';
	j = i;

	myLog << ParentWidth;
	myLog << ParentHeight;

	Width = atoi(ParentWidth); // canvas size
	Height = atoi(ParentHeight);

}

// update message
void BlobFollower::SetEffectData(char* Data) {

	char delimiter = ';';
	char divider = ',';
	int i = 0;
	int j = 0;

	char r[32]; // float 1 ...
	i = j;
	while (Data[i] != divider){
		r[i - j] = Data[i];
		i++;
		}
	r[i - j] = '\0';
	i++;
	j = i;

	char g[32]; // float 2 ...
	i = j;
	while (Data[i] != divider){
		g[i - j] = Data[i];
		i++;
		}
	g[i - j] = '\0';
	i++;
	j = i;

	char b[32]; // float 3 ...
	i = j;
	while (Data[i] != divider){
		b[i - j] = Data[i];
		i++;
		}
	b[i - j] = '\0';
	i++;
	j = i;

	char opacity[32]; // float 4 ...
	i = j;
	while (Data[i] != divider){
		opacity[i - j] = Data[i];
		i++;
		}
	opacity[i - j] = '\0';
	i++;
	j = i;

	char radius[32]; // float 5 ...
	i = j;
	while (Data[i] != divider){
		radius[i - j] = Data[i];
		i++;
		}
	radius[i - j] = '\0';
	i++;
	j = i;

	char elements[32];
	i = j;
	while (Data[i] != divider){
		elements[i - j] = Data[i];
		i++;
		}
	elements[i - j] = '\0';
	i++;
	j = i;

	int num = (int)atof(elements);

	param1 = atof(r);
	param2 = atof(g);
	param3 = atof(b);
	param4 = atof(opacity);
	param5 = atof(radius);

	mSizes.clear();
	mLocations.clear();

	if(num == 0) return;

	for(int element = 0; element < num; element++) {
		
		char coordinatex[32]; // X
		i = j;
		while (Data[i] != divider){
			coordinatex[i - j] = Data[i];
			i++;
			}
		coordinatex[i - j] = '\0';
		i++;
		j = i;

		char coordinatey[32]; // Y
		i = j;
		while (Data[i] != divider){
			coordinatey[i - j] = Data[i];
			i++;
			}
		coordinatey[i - j] = '\0';
		i++;
		j = i;

		char sizex[32]; // Width (percent)
		i = j;
		while (Data[i] != divider){
			sizex[i - j] = Data[i];
			i++;
			}
		sizex[i - j] = '\0';
		i++;
		j = i;

		char sizey[32]; // Height (percent)
		i = j;
		while (Data[i] != divider){
			sizey[i - j] = Data[i];
			i++;
			}
		sizey[i - j] = '\0';
		i++;
		j = i;

		// center locations
		mLocations.push_back(
			Point(
			(double)atof(coordinatex), 
			(double)atof(coordinatey)));

		// size pairs
		mSizes.push_back(
			Point(
			(double)atof(sizex), 
			(double)atof(sizey)));		


	}
}

void BlobFollower::Draw() {

	isLocked = true;

	gl::disable(GL_TEXTURE_2D);
	glColor4f(param1, param2, param3, param4);
	gl::enableAlphaBlending();

	int mindex = 0;
	std::vector<Point>::iterator PointIterator = mLocations.begin();
	while(PointIterator != mLocations.end()) {
		
		float drawOffsetX = ((PointIterator->GetX() - 0.5) * Width - float(mSizes[mindex].GetX() * Width) / 2.0f);
		float drawOffsetY = ((PointIterator->GetY() - 0.5) * Height - float(mSizes[mindex].GetY() * Height) / 2.0f);	

		gl::drawStrokedRect(Rectf(drawOffsetX, drawOffsetY, drawOffsetX + float(mSizes[mindex].GetX() * Width), drawOffsetY + float(mSizes[mindex].GetY() * Height)));

		mindex++;
		std::advance(PointIterator,1);

	}

	gl::disableAlphaBlending();
	gl::enable(GL_TEXTURE_2D);

	isLocked = false;

}