#pragma once

//#define DEV_MODE

#ifdef DEV_MODE
#include "cinder/app/AppNative.h"
#endif

#include <iostream>
#include <fstream>

#include "IEffectInterface.h"

#include "cinder/gl/gl.h"
#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "ParticleEmitter.h"
#include "cinder/Timeline.h"
#include "cinder/gl/TextureFont.h"
#include "rapidjson.h"
#include "document.h"
#include "PixelCoordinate.h"

using namespace ci;
using namespace ci::app;
using namespace std;

#ifdef DEV_MODE
class FresnoBaseProjectApp : public AppNative, public IEffectInterface	 {
#else
class FresnoBaseProjectApp : public IEffectInterface	 {
#endif
  public:
	FresnoBaseProjectApp();
	void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();

	void Update();
	void Draw();

	void Initialize(char* InitData, Timeline* CinderTimeline);
	void SetEffectData(char* Data);
	void InitDevMode();

	int splitPairs(const std::string &s, char delimiter);
	void splitPairData(const std::string &s, char delimiter);

	std::vector<Point> pixelCoordinates;

private:

	int counter;
	float redVal, blueVal, greenVal;

	gl::Texture myTexture;	
	float canvasWidth, canvasHeight;
	float canvasHalfWidth, canvasHalfHeight;

	//de-normalize coordinates
	Vec2f getWorldCoords();
	void setCanvasSizeVars(float w, float h);
	ParticleEmitter pe;
	void globalSetup(); //setup common to both dev and production DLL modes	

	Timeline* privateTimeline;// = CinderTimeline;
	float deltaTime;
	float lastRecordedTime;
	void setDeltaTime();

	void drawDebugInfo();
	const std::string CurrentDateTime(void);

	gl::TextureFontRef	mTextureFont;
	Font mFont;

	float param1, param2, param3, param4, param5; 
	std::vector<Point> mLocations;
	std::vector<Point> mSizes;

	int blobCountHack;

	//umbrella emitters
	std::vector<Point> umbrellaPositions;

	void drawUmbrellaPositions();
};